//= require jquery/dist/jquery.js

//= require owl.carousel/dist/owl.carousel.js
//= require helpers/jquery.matchHeight.js
//= require svg4everybody/dist/svg4everybody.js

//= require toggle-menu/toggle-menu.js
//= require header/header.js
//= require popup/popup.js
//= require slider-reviews/slider-reviews.js
//= require slider-gallery/slider-gallery.js
//= require subscribe/subscribe.js

$(function () {

  var equalHeight = $('.js-equal-height');

  equalHeight.each(function () {
    var t = $(this);
    var childs = t.children();
    childs.matchHeight();
  })

  svg4everybody();
})
