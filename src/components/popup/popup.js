$(function () {

  var overlay = $('.overlay');
  var modals = $('.target-modal');

  $('.open-modal').each(function (i) {
    var openModal = $(this);
    var target = openModal.data('modal');
    var targetModal = $('.target-modal[data-modal=' + target + ']');

    openModal.click(function () {

      if ($(window).height() < targetModal.innerHeight()) {
        targetModal.css({ height:'95%', overflowY: 'scroll' });
      }

      if ($(window).width() < targetModal.innerWidth()) {
        targetModal.css({ width:'95%'});
      }

      modals.fadeOut(100);
      targetModal.fadeIn(100);
      overlay.fadeIn(100);

      return false;
    })

  })

  overlay.click(function () {
    overlay.fadeOut(100);
    modals.fadeOut(100);
  })

  $('.popup').each(function () {
    var popup = $(this);
    var close = popup.find('.close');

    close.click(function () {
      popup.fadeOut(100);
      overlay.fadeOut(100);
    })
  })
})
