$(function () {

  var toggleMenu = $('.js-toggle-menu');

  toggleMenu.on('click', toggleMenuClick);



  function toggleMenuClick() {

    var header = $('.header');
    var dropdownMenu = $('.js-dropdown-menu');
    var dropdownInner = dropdownMenu.find('.dropdown-menu__inner');
    var dropdownInnerHeight = dropdownInner.innerHeight();
    var toggle = $(this);

    toggle.toggleClass('active');
    dropdownMenu.toggleClass('active');
    header.toggleClass('active');

    if (dropdownMenu.hasClass('active')) {
      dropdownMenu.height(dropdownInnerHeight);
    } else {
      dropdownMenu.height(0);
    }

    return false;
  }

})
