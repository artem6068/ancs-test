$(function () {

  var reviews = $('.js-reviews');
  var navContainer = $('.reviews__buttons');
  var dotsContainer = $('.reviews__dots');
  var dots = $('.reviews__dot');
  var speed = 400;

  var navPrevText = '<svg class="icon icon_arrow-slider-left"><use xlink:href="/static/img/svg/svg-symbols.svg#icon-arrow-slider-left"></use></svg>';

  var navNextText = '<svg class="icon icon_arrow-slider-right"><use xlink:href="/static/img/svg/svg-symbols.svg#icon-arrow-slider-right"></use></svg>';

  reviews.owlCarousel({
    loop: false,
    margin: 30,
    items: 1,
    startPosition: 1,
    navContainer: navContainer,
    dotsContainer: dotsContainer,
    nav: true,
    navClass: ['reviews__prev', 'reviews__next'],
    navText: [navPrevText, navNextText],
    navSpeed: speed
  })

  dots.on('click', dotClick)

  function dotClick() {

    var dot = $(this);
    var dotIndex = dot.index();

    reviews.trigger('to.owl.carousel',[dotIndex, speed]);

    dots.removeClass('active');
    dot.addClass('active');
  }

})
