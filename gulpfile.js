var gulp = require('gulp');
var browserSync = require('browser-sync');
require('require-dir')('tasks');

gulp.task('serve', function () {
  browserSync.init({
    server: "./app"
  });
})

gulp.task('watch', () => {
  gulp.watch(['./src/components/**/*.json', './src/components/**/*.pug', './src/**/*.pug'], gulp.series('pug:json', 'pug'));
  //gulp.watch('./src/**/*.sass', gulp.series('sass'));
  gulp.watch('./src/**/*.styl', gulp.series('stylus'));
  gulp.watch('./src/**/*.js', gulp.series('js'));
  gulp.watch('./src/static/img/**/*', gulp.series('copy:images', 'sprite'));
  gulp.watch('./src/static/fonts/*', gulp.series('copy:fonts'));
});

gulp.task('default', gulp.series(
  gulp.parallel('pug:json'),
  gulp.parallel('copy:images', 'sprite', ),
  //gulp.parallel('pug', 'sass', 'js'),
  gulp.parallel('pug', 'stylus', 'js'),
  gulp.parallel('watch', 'serve')
));
