var gulp = require('gulp');
var browserSync = require('browser-sync');
var notify = require('gulp-notify');
var concat = require('gulp-concat');

//var sass = require('gulp-sass');
var bootstrap = require('bootstrap-styl');
var stylus = require('gulp-stylus');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');

var sourcemaps = require('gulp-sourcemaps');


// gulp.task('sass', function () {
//   return gulp
//     .src('./src/static/sass/main.sass')
//     .pipe(sourcemaps.init())
//       .pipe(sass())
//       .on("error", notify.onError({
//         message: "<%= error.message %>",
//         title: "Error sass"
//       }))
//       .pipe(autoprefixer({
//         browsers: ['last 4 versions']
//       }))
//       //.pipe(csso({}))
//     .pipe(sourcemaps.write('./'))
//     .pipe(gulp.dest('./app/static/css/'))
//     .pipe(browserSync.reload({
//       stream: true
//     }));
// });

gulp.task('stylus', function () {
  return gulp
    .src('./src/static/stylus/main.styl')
    .pipe(sourcemaps.init())
      .pipe(stylus({
        'use': bootstrap(),
        'include css': true,
        'include': ['./node_modules', './src/components', './src/sections']
      }))
      .on("error", notify.onError({
        message: "<%= error.message %>",
        title: "Error stylus"
      }))
      .pipe(autoprefixer({
        browsers: ['last 4 versions']
      }))
    //.pipe(csso({}))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./app/static/css/'))
    .pipe(browserSync.reload({
      stream: true
    }));
});
