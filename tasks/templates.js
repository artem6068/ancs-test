var gulp = require('gulp');
var browserSync = require('browser-sync');

var pug = require('gulp-pug');
var mergeJson = require('gulp-merge-json');
var data = require('gulp-data');

var notify = require('gulp-notify');
var fs = require('fs');


gulp.task('pug', function () {
  return gulp
    .src('./src/pages/*.pug')
    .pipe(data(file => {
      return JSON.parse(fs.readFileSync('./src/data.json'));
    }))
    .pipe(pug({
      pretty: true,
    }))
    .on("error", notify.onError({
      message: "<%= error.message %>",
      title: "Error pug"
    }))
    .pipe(gulp.dest('./app'))
    .pipe(browserSync.reload({
      stream: true
    }));
})

gulp.task('pug:json', function () {
  return gulp
    .src(['./src/components/**/*.json'])
    .pipe(mergeJson({
      fileName: 'data.json'
    }))
    .pipe(gulp.dest('./src/temp'))
    .pipe(browserSync.reload({
      stream: true
    }));
})
