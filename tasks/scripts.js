var gulp = require('gulp');
var browserSync = require('browser-sync');
var notify = require('gulp-notify');
var uglify = require('gulp-uglify');
var include = require('gulp-include');

var sourcemaps = require('gulp-sourcemaps');


gulp.task('js', function () {
  return gulp
    .src('./src/static/js/main.js')
    .pipe(sourcemaps.init())
      .pipe(include({
        extensions: "js",
        hardFail: true,
        includePaths: [
          "./node_modules",
          "./src/components",
          "./src/sections",
          "./src/static/js"
        ]
      }))
      .on("error", notify.onError({
        message: "<%= error.message %>",
        title: "Error js"
      }))
      //.pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./app/static/js/'))
    .pipe(browserSync.reload({
      stream: true
    }));
});
