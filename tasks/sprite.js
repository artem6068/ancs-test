var gulp = require('gulp');
var gulpIf = require('gulp-if');

var svgSymbols = require('gulp-svg-symbols');

var svgmin = require('gulp-svgmin');
var	cheerio = require('gulp-cheerio');
var	replace = require('gulp-replace');

gulp.task('sprite', function () {
    return gulp
      .src('./src/static/img/svg/icons/*.svg')
      .pipe(svgmin({
  			js2svg: {
  				pretty: true
  			}
  		}))
  		.pipe(cheerio({
  			run: function ($) {
  				$('[fill]').removeAttr('fill');
  				$('[stroke]').removeAttr('stroke');
  				$('[style]').removeAttr('style');
  			},
  			parserOptions: {xmlMode: false}
  		}))
  		.pipe(replace('&gt;', '>'))
      .pipe(svgSymbols({
        title: false,
  			id: 'icon-%f',
  			class: '.icon_%f',
      }))
      .pipe(gulpIf('*css', gulp.dest('./src/components/icon') ,gulp.dest('./app/static/img/svg')));
});
