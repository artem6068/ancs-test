var gulp = require('gulp');
var browserSync = require('browser-sync').create();

gulp.task('copy:images', function () {
  return gulp
    .src('./src/static/img/**/*')
    .pipe(gulp.dest('./app/static/img/'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('copy:fonts', function () {
  return gulp
    .src('./src/static/fonts/**/*')
    .pipe(gulp.dest('./app/static/fonts/'))
    .pipe(browserSync.reload({
      stream: true
    }));
});
